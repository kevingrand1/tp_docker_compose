const express = require('express');
const mongoose = require('mongoose');

mongoose.connect('mongodb://mongo:27017/myapp', { useNewUrlParser: true });

const User = mongoose.model('User', {
    name: String,
    age: Number
});

const app = express();

app.get('/api/users', (req, res) => {
    User.find((err, users) => {
        if (err) return res.status(500).send(err);
        res.send(users);
    });
});

app.listen(3000, () => {
    console.log('Backend listening on port 3000!');
});
