# Objectifs
L’objectif est de réaliser une application multiservices comprenant les services suivants :
* un proxy nginx
* un backend avec NodeJS
* un frontend avec VueJS (ou Angular) une base de données avec MongoDB
* Vous opterez aussi pour une stratégie pertinente d’isolation réseau ainsi qu’une stratégie de persistance.

# Contenu
L’objectif est d’avoir une application de démonstration montrant le lien entre chacun des services. Peu importe le contenu, il faut une page permettant d’avoir le frontend, appelant le backend (via le proxy), appelant lui-même un élément de la base de données MongoDB.



# Installation : 

 - docker-compose up -d

